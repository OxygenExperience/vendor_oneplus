# Copyright 2018 Oxygen Experience
# Every proprietary file is property of their respective authors.
# This is a non-profit work.

#APKs
PRODUCT_PACKAGES += \
	OnePlusCamera \
	OnePlusGallery \
	OnePlusLauncher \
        OnePlusWidget \
	OPIconpackCircle \
	OPIconpackOnePlus \
	OPIconpackSquare \
	OPSoundRecorder \
	Weather

#Bootanimation
PRODUCT_COPY_FILES += \
	vendor/oneplus/oxygen/prebuilts/OnePlusBootanimation/bootanimation.zip:system/media/bootanimation.zip

